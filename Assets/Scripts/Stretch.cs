﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// The script for object stretching
/// </summary>
[ExecuteInEditMode]
public class Stretch : MonoBehaviour
{
	/// <summary>
	/// Whether to stretch the X-axis.
	/// </summary>
	public bool axisX = true;

	/// <summary>
	/// Whether to stretch the Y-axis.
	/// </summary>
	public bool axisY = true;

	/// <summary>
	/// The relative value of the strech for X and Y axes.
	/// </summary>
	public Vector2 relative = Vector2.one;

	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	public void OnEnable()
	{
		Resize();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void Update()
	{
#if UNITY_EDITOR
		Resize();
#endif
	}

	/// <summary>
	/// Update scale of the object.
	/// </summary>
	public void Resize()
	{
		var scale = transform.localScale;
		if (axisX) scale.x = Screen.width * relative.x;
		if (axisY) scale.y = Screen.height * relative.y;
		transform.localScale = scale;
	}
}
