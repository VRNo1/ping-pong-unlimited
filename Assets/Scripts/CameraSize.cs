﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// The orthographic camera script.
/// </summary>
[ExecuteInEditMode]
public class CameraSize : MonoBehaviour
{
	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	public void OnEnable()
	{
		Resize();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void Update()
	{
#if UNITY_EDITOR
		Resize();
#endif
	}

	/// <summary>
	/// Update orthographic size of the camera to match screen height.
	/// </summary>
	public void Resize()
	{
		camera.orthographicSize = Screen.height / 2f;
	}
}
